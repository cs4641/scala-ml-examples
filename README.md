# Machine Learning Examples Using Scala and Spark

Spark jobs are in the `edu.gatech.cs4641.spark` package.  Plain 'ol Scala programs are in the `edu.gatech.cs4641` package.  All data sets used by the programs in this project are in the `data` folder.

## Running Spark Jobs

To run a Spark job:

1. Compile and package the code with `sbt package`.  This puts a compiled Jar archive in `target/scala-2.12/scala-ml-examples.jar`
2. Submit a job to Spark for running.  You'll need to specify

    - `--class` - the name of a Scala application to run, that is an `object` with a `main` method.
    - `--packages` - extra packages required by the application you're running that aren't already on Spark's classpath.  For example, many applications use `breeze-viz`.
    - `--master local[n]` -- `n` is the number of cores for Spark to use to run your job (also, assuming you're running in local mode, not on a cluster).
    - The Jar archive containing your application (created by `sbt package`).
    - Any command-line arguments to your application.  All of the examples take a command-line argument specifying the data file to read.
    
Here's an example: the `BreastCancer` app that runs logistic regression on the Wisconsin Diagnostic Breast Cancer data set.    

```bash
sbt package
spark-submit --class "edu.gatech.cs4641.spark.BreastCancer" \
    --packages org.scalanlp:breeze-viz_2.12:1.0-RC2 \
    --master local[1] target/scala-2.12/scala-ml-examples_2.12-0.1.jar \
    data/wdbc.data
```

