package edu.gatech.cs4641

import breeze.plot.{Figure, plot}
import org.jfree.chart.axis.NumberTickUnit

class FakeFingerprints {

  def main(args: Array[String]): Unit = {

    // Create a plot of the ROC curve using breeze-viz
    val fig = Figure()
    // Headless -- we're only using Figure for the saveas method
    fig.visible = false
    val plt = fig.subplot(0)

    val xs = breeze.linalg.linspace(0, 1, 100)
    val ys = breeze.linalg.linspace(0, 1, 100)
    val zs = breeze.linalg.linspace(0, 1, 100)
    fig.saveas("breast-cancer-roc.png")


  }
}
