package edu.gatech.cs4641.spark

import org.apache.spark.sql.SparkSession

object SocoFing {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder.appName("SOCOFing").getOrCreate()

    val df = spark.read
      .format("image")
      .load("file:///Users/cs257/Downloads/data/kaggle/socofing/SOCOFing/Real/")

    df.printSchema()

    spark.stop()
  }
}
