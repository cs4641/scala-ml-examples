package edu.gatech.cs4641.spark

import breeze.plot.{Figure, plot}
import org.apache.spark.ml.classification.{BinaryLogisticRegressionSummary, LogisticRegression, LogisticRegressionModel}
import org.apache.spark.ml.feature.{StringIndexer, VectorAssembler}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.jfree.chart.axis.NumberTickUnit

/**
  * Spark applciation to run logistic regression on the
  * [[https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+(Diagnostic) Wisconsin Diagnostic Breast Cancer Data]]
  *
  * Requires breeze-viz to create ROC curve.
  */
object BreastCancer {
  def main(args: Array[String]) {
    val spark = SparkSession.builder.appName("Breast Cancer").getOrCreate()

    val df = spark.read
      .option("inferSchema", "true")
      .option("header", "false")
      .csv(args(0))

    // DataFrame transformer to create a column containing the label,
    // which for LogisticRegression must be numeric.
    val labelIndexer = new StringIndexer()
      .setInputCol("_c1")
      .setOutputCol("label")

    // DataFrame transformer to create a column containing the feature vectors
    val assembler = new VectorAssembler()
      .setInputCols(Array("_c2", "_c3", "_c4", "_c5", "_c6", "_c7", "_c8", "_c9", "_c10",
    "_c11", "_c12", "_c13", "_c14", "_c15", "_c16", "_c17", "_c18", "_c19", "_c20",
    "_c21", "_c22", "_c23", "_c24", "_c25", "_c26", "_c27", "_c28", "_c29", "_c30", "_c31"))
      .setOutputCol("features")


    // Apply transformers to get produce a DataFrame ready for the learner
    val indexed: DataFrame = labelIndexer.fit(df).transform(df)
    val assembled: DataFrame = assembler.transform(indexed)

    // 80% training data, 20% for test data
    val Array(trainingData, testData) = assembled.randomSplit(Array(0.8, 0.2))

    // The model class.  You must tell it which column contains the feature
    // vectors and which column contains the labels
    val lr = new LogisticRegression()
      .setLabelCol("label")
      .setFeaturesCol("features")

    // Fit the model class to the training data to produce a model instance
    val model: LogisticRegressionModel = lr.fit(trainingData)

    // Evaluate the model on the test data
    val testResults: BinaryLogisticRegressionSummary =
      model.setThreshold(0.5).evaluate(testData).asBinary
    val testResults1: BinaryLogisticRegressionSummary =
      model.setThreshold(0.8).evaluate(testData).asBinary
    val testResults2: BinaryLogisticRegressionSummary =
      model.setThreshold(0.2).evaluate(testData).asBinary

    // Print evaluation metics
    for ((th, tr) <- List(0.5, 0.8, 0.2).zip(List(testResults, testResults1, testResults2))) {
      println(s"With classification threshold of ${th}:")
      println(s"Accuracy: ${tr.accuracy}")
      println(s"TPR: ${tr.weightedTruePositiveRate}")
      println(s"FPR: ${tr.weightedFalsePositiveRate}")
      println(s"AUC: ${tr.areaUnderROC}")
      println("ROC:")
      tr.roc.show()
    }

    // Create a plot of the ROC curve using breeze-viz
    val fig = Figure()
    // Headless -- we're only using Figure for the saveas method
    fig.visible = false
    val plt = fig.subplot(0)

    // Extracting the values from a Spark DataFrame requires the implicit
    // conversions in import spark.implicits._ to convert from Spark's
    // internal data types to Scala data types
    import spark.implicits._
    val xs = testResults.roc.select("FPR").as[Double].collect()
    val ys = testResults.roc.select("TPR").as[Double].collect()

    // ROC Curve
    plt += plot(xs, ys)

    // Baseline
    plt += plot(Array(0.0, 1.0), Array(0.0, 1.0))

    plt.xlabel = "False Positive Rate"
    plt.ylabel = "True Positive Rate"
    plt.xaxis.setTickUnit(new NumberTickUnit(0.1))
    plt.yaxis.setTickUnit(new NumberTickUnit(0.1))
    fig.saveas("breast-cancer-roc.png")

    spark.stop()
  }
}
